<?php

class TemplateManager
{
    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);
        $replaced->subject = $this->computeText($replaced->subject, $data);
        $replaced->content = $this->computeText($replaced->content, $data);

        return $replaced;
    }

    private function computeText($text, array $data)
    {

        $summaryHtml = '';
        $summary = '';
        $destinationName = '';
        $destinationLink = '';

        /*
         * USER
         * [user:*]
         */
        $APPLICATION_CONTEXT = ApplicationContext::getInstance();

        if (isset($data['user'])  and ($data['user']  instanceof User)) {
            $user = $data['user'];
        } else {
            $user = $APPLICATION_CONTEXT->getCurrentUser();
        }

        if ($user) {
            $userFirstName = ucfirst(mb_strtolower($user->firstname));
        }

        /*
         * Quote
         * [quote:*]
         */
        if (isset($data['quote']) and $data['quote'] instanceof Quote) {
            $quote = $data['quote'];

            $quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $siteFromRepository = SiteRepository::getInstance()->getById($quote->siteId);
            $destinationFromRepository = DestinationRepository::getInstance()->getById($quote->destinationId);

            $summaryHtml = $quote->renderHtml($quoteFromRepository);
            $summary = $quote->renderText($quoteFromRepository);
            $destinationName = $destinationFromRepository->countryName;
            $destinationLink = $siteFromRepository->url . '/' . $destinationFromRepository->countryName . '/quote/' . $quoteFromRepository->id;
        }

        $search = [
            '[user:first_name]',
            '[quote:summary_html]',
            '[quote:summary]',
            '[quote:destination_name]',
            '[quote:destination_link]'
        ];

        $text = str_replace(
            $search, 
            [$userFirstName, $summaryHtml, $summary, $destinationName, $destinationLink], 
            $text
        );

        return $text;
    }
}
